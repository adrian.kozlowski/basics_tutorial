package arrays;

import java.util.Random;

public class ArraysRandom {
    /**
     * qwertyuiop
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] tab = new int[10]; //10 elementowa tablica intów
        int max = 0, min = 0;
        double averageSum = 0;
        int amountDigitsBiggerThanAverage = 0;
        Random random = new Random();
        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextInt(21) - 10;
            averageSum += tab[i];
            if (tab[i] > max) {
                max = tab[i];
            }
            if (tab[i] < min) {
                min = tab[i];
            }
            System.out.println(tab[i]);
        }
        double avg = averageSum / (double) tab.length;
        for (int i : tab) {
            if (i > avg) {
                amountDigitsBiggerThanAverage++;
            }
        }

        System.out.println(new ArraysRandom());
    }

    @Override
    public String toString() {
        return "Bułka z masłem";
    }
}
