package arrays;

/**
 * Created by Adrian on 10.01.2017.
 */
public class Sphere {
    private double diameter;

    public Sphere(double diameter) {
        this.diameter = diameter;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return Double.toString(this.diameter);
    }
}
