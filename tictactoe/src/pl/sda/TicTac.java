package pl.sda;

/**
 * Created by Adrian on 21.12.2016.
 */
public class TicTac {

    static int[][] board = new int[3][3];

    //
    //
    //
    //


    public static void main(String... args) {

        System.out.println("3 2");
//   System.out.println("3 kolumna i 2 wiersz");

        board[2][1] = 2; //2 symbolizuje X, 1 symbolizuje O

        for (int y = 0; y <= 2; y++) {          //weź wiersz
            for (int x = 0; x <= 2; x++) {      //weź kolumnę
                if (board[x][y] == 0) {         //jeśli komórka jest równa zero
                    System.out.print(" ");      //zostaw pustą
                } else if (board[x][y] == 1) {  //jeśli jednak komórka jest równa 1
                    System.out.print("O");      //wypisz O
                } else if (board[x][y] == 2) {  //jeśli jednak komórka jest równa 2
                    System.out.print("X");      //wypisz X
                }
                if (x == 2) {                   //jeśli rozważamy ostatnią kolumnę, nie potrzebujemy już pisać pionowej kreski
                    continue;                   //przejdź do następnej iteracji
                }
                System.out.print("|");          //napisz pionową kreskę
            }
            System.out.println();               //przejdź do nowej linii
            if (y != 2) {                       //jeśli rozważamy ostatni wiersz nie potrzebujemy już pisać poziomej kreski
                System.out.print("-----");      //wypisz poziomą kreskę
                System.out.println();           //przejdź do nowej linii
            }
        }

//        System.out.println("  | |  ");
//        System.out.println(" ----- ");
//        System.out.println("  | |X");
//        System.out.println(" ----- ");
//        System.out.println("  | | ");
    }
}
