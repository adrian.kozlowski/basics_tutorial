package sda;

import java.util.Date;

public class Human {
    private String name;
    private String lastName;
    private Date dateOfBirth;
    private String city;
    private String country;

    public Human() {
    }

    public Human(String name, String lastName, Date dateOfBirth, String city, String country) {
        this.name = name;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.city = city;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public Human setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Human setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public Human setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Human setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Human setCountry(String country) {
        this.country = country;
        return this;
    }
}
