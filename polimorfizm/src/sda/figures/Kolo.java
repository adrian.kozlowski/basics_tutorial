package sda.figures;

/**
 * Created by Adrian on 09.01.2017.
 */
public class Kolo  extends  Figura{
    final static float PI = 3.1415f;

    public Kolo(float promien){
        this.bokA = promien;
    }

    @Override
    public float pole() throws RownaZeroException, MniejszaOdZeraException{
//        Math.pow(bokA,2);
        return bokA*bokA*PI;
    }
}
