import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {

    static ArrayList<User> userCollection = new ArrayList<User>();

    /**
     * Sugeruję utworzyć:
     * menu główne w postaci:
     * 1. dodaj użytkowników
     * dodawane będą imię, nazwisko, wiek, płeć
     * 2. wyświetl uzytkowników
     * Wyświetlana będzie lista w postaci:
     * 1. Imię Nazwisko : xx lat
     * 2. Imię Nazwusko : xx lat
     * 3. wczyaj użytkowników z pliku (1 plik per user)
     * 4. wystaw fakturę
     * 5. wczytaj fakturę z pliku
     * 6. zamknij program
     * <p>
     * <p>
     * na fakturze powinna być możliwość dodania osobno sprzedawcy jak i kupującego
     * dodawanie przedmiotów, które zostały zakupione
     * faktura wyśtwietla się:
     * a) wypunktowane pozycje, ich ceny i ilość
     * b) na dole faktury obliczona jest suma
     * c) zapisać fakturę do pliku (dowolny sposób prezentacji danych)
     * <p>
     * <p>
     * Wskazówki:
     * proponuję podzielić program na 6 metod public static void, które będą wykonywane w momnecie wybrania z menu odpowiedniej liczby.
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //tak tworzy się obiekt do odczytywania konsoli
        // rozbudowany if: w zależności od tego, jaką wartość wpiszemy z klawiatury swich wybierze odpowiednią metodę:
        //dodatkowo opakowałbym tego switcha w pętlę, która nie pozwoli zamknąć programu dopóki ten nie odczyta 6.
        readUsersFromFile();
        while (true) {
            System.out.println("Menu główne");
            System.out.println("1. dodaj użytkowników");
            System.out.println("2. wyświetl uzytkowników");
            System.out.println("3. wczyaj użytkowników z pliku (1 plik per user)");
            System.out.println("4. wystaw fakturę");
            System.out.println("5. wczytaj fakturę z pliku");
            System.out.println("6. zamknij program");
            int chosen = new Integer(scanner.nextLine());   // program czeka aż użytkownik coś wpisze w konsoli
            switch (chosen) {                               //sprawdzamy co zostało wpisane do choosen
                case 1:                                     //gdy zostanie wybrany 1 to:
                    addUsers(scanner);                      //wywołaj funkcję addUsers z parametrem scanner
                    break;                                  //przerwyj wywołanie switch
                case 2:                                     //gdy zostanie wybrana 2 to:
                    showUsers();                            //wywołaj funkcje showUsers
                    break;                                  //przerwyj wywołanie switch
                case 3:                                     //gdy zostanie wybrana 3 to:
                    readUsersFromFile();                    //wywołaj funkcję readUsersFromFile
                    break;                                  //przerwyj wywołanie switch
                case 4:                                     //gdy zostanie wybrana 4 to:
                    invoice(scanner);                       //wywołaj funkcję invoice
                    break;                                  //przerwyj wywołanie switch
                case 5:                                     //gdy zostanie wybrana 5 to:
                    readInvoiceFromFile();                  //wywołaj readInvoiceFromFile
                    break;                                  //przerwyj wywołanie swich
                case 6:                                     //gdy zostanie wybrana 6 to:
                    return;                     //return kończy funkcję lub zwraca wartość. W tym przypadku main nie zwraca wartości (void) zatem kończy funkcję programu/
            }
        }
    }

    private static void readInvoiceFromFile() {
        System.out.println("Ta funkcjonalność dopiero będzie rozwijana");
        //not implemented yet
    }

    private static void invoice(Scanner scanner) {
        System.out.println("Wystawianie faktury:");             //Wypisz "wystawianie faktury"
        System.out.println("Wybierz indeks sprzedawcy:");       //Wypisz "Wybierz indeks sprzedawcy"
        showUsers();                                            //wywołaj funkcję showUsers by pokazać dostępnych użytkownikow
        int sellerIndex = new Integer(scanner.nextLine());      //wczytaj z konsoli index sprzedawcy scanner.nextLine() zwraca ciąg znaków natomiast nam zależy na liczbie całkowitej dlatego tworzymy obiekt typu integer
        System.out.println("Wybierz indeks kupującego");        //Wypisz "Wybierz index kupujacego"
        showUsers();                                            //Wywołaj funkcję showUsers by pokazać dostępnych użytkowników
        int buyerIndex = new Integer(scanner.nextLine());       //wczytaj z konsoli index kupującego
        Document document = new Document();                     //Alokujemy pamięć na document
        document.setBuyer(userCollection.get(buyerIndex));      //ustawiamy kupującego wybierajać go z kolekcji po indexie
        document.setSeller(userCollection.get(sellerIndex));    //ustawiamy sprzedającego
        int i = 0;                                              //inicjalizujemy zmienna pomocniczą
        while (i != 2) {                                        //powtarzaj pętlę dopóki i jest różne od 2
            System.out.println("1. Dodaj pozycję do faktury");  //wyświetl na konsoli "Dodaj pozycję do faktury"
            System.out.println("2. Wystaw fakturę");            //wyświetl na konsoli "Wystaw fakturę"
            i = new Integer(scanner.nextLine());                //wczytaj z konsoli wpisany index i przypisz do i
            if (i == 1) {                                       //jeżeli i jest równe 1 to
                document.addItem(addInvoiceItem(scanner));             //wywołaj funkcję addInvoiceItem(), która zwraca Item. jednocześnie dodając go do kolekcji itemów w dokumencie
            }
        }
        String stringInvoice = composeInvoice(document);        //wywołaj funkcję composeInvoice z parametrem document
        saveInvoiceToFile(stringInvoice);                       //wywołaj funkcję saveInvoiceToFile z parametrem stringInvoice
        printOnScreen(stringInvoice);                           //wywołaj funkcję printOnScreen z parametrem stringInvoice
    }

    private static void printOnScreen(String stringInvoice) {
        System.out.print(stringInvoice);                        //pokaz stringInvoice na konsoli
        System.out.println();                                   //wyświetl dodatkową pusta linię
    }

    /**
     * metoda komponuje ciąg znaków tak by wyglądaj jak faktura:
     * <p>
     * <p>
     * ---------------------------------------------------------
     * |Kupujący                                    Sprzedający|
     * | Adrianna                    Adrian                    |
     * | Janik                       Kozłowski                 |
     * | 25                          12                        |
     * |-------------------------------------------------------|
     * | LP  NAZWA                      ILOŚĆ       CENA       |
     * | 1.  Nazwa itemu                1           9,99       |
     * |-------------------------------------------------------|
     * TOTAL: 9,99
     * ---------------------------------------------------------
     *
     * @param document
     * @return
     */
    private static String composeInvoice(Document document) {
        StringBuilder invoiceString = new StringBuilder();
        invoiceString.append("---------------------------------------------------------\n");
        invoiceString.append("|Kupujący                                    Sprzedający|\n");
        //formatowanie pozwala na zarezerwowanie odpowiedniej ilości znaków w linii.
        invoiceString.append(String.format("| %-25s   %-25s |%n", document.getBuyer().getName(), document.getSeller().getName()));
        invoiceString.append(String.format("| %-25s   %-25s |%n", document.getBuyer().getSurname(), document.getSeller().getSurname()));
        invoiceString.append(String.format("| %-25s   %-25s |%n", document.getBuyer().getAge(), document.getSeller().getAge()));
        invoiceString.append("|-------------------------------------------------------|\n");
        int order = 1;
        float sum = 0;
        invoiceString.append(String.format("| %-3s %-26s %-10s  %-10s |%n", "LP", "NAZWA", "ILOŚĆ", "CENA"));
        for (Item item : document.getItems()) {
            invoiceString.append(String.format("| %-3s %-26s %010.2f  %010.2f |%n", order++, item.getName(), item.getQty(), item.getPrice()));
            sum += item.getPrice();
        }
        invoiceString.append("|-------------------------------------------------------|\n");
        invoiceString.append(String.format("TOTAL: %.2f %n", sum));
        invoiceString.append("---------------------------------------------------------\n");

        return invoiceString.toString();
    }

    private static void saveInvoiceToFile(String document) {
        File invoiceDirectory = new File("invoices");
        if (!invoiceDirectory.exists()) {
            invoiceDirectory.mkdir();
        }

        //uchwyt do nowego pliku faktury gdzie jego nazwa to aktualny czas i rozszerzenie .inv
        File invoice = new File(invoiceDirectory.getAbsolutePath() + "/" + System.currentTimeMillis() + ".inv");
        try {
            // otwórz strumień do pliku
            BufferedWriter writer = new BufferedWriter(new FileWriter(invoice));
            writer.write(document); //zapisz do pliki sformatowany ciąg znaków
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Item addInvoiceItem(Scanner scanner) {
        Item item = new Item();                         //alokujemy miejsce w pamięci na Item
        System.out.println("Podaj nazwę");              //wyświetl "podaj nazwę"
        item.setName(scanner.nextLine());               //Wczytaj nazwę z konsoli
        System.out.println("Podaj ilość");              //Wyswietl "podaj ilość"
        item.setQty(new Float(scanner.nextLine()));     //wczytaj ilość i zmien typ na float
        System.out.println("Podaj cenę");               //wyświetl "podaj cene"
        item.setPrice(new Float(scanner.nextLine()));   //wczyaj cenę i zmien typ na float
        return item;                                    //zwróć obiekt item
    }

    private static void readUsersFromFile() {
        File file = new File(".");                      //kropka to obecny katalog
        File[] files = file.listFiles(new FilenameFilter() {    //pobierz wszystkie pliki, które istnieją w katalogu . (kropka)
            @Override
            //i zaaplikuj odpowiedni filtr tak aby zosały wybrane tylko takie pliki
            public boolean accept(File dir, String name) {      //które spełniają warunek
                return name.endsWith(".usr");                   //
            }
        });

        for (File userFile : files) {                           //iteruj przez wszystkie pobrane pliki
            try {
                String line = "";                               //inicializaja pustej zmiennej typu string
                BufferedReader reader = new BufferedReader(new FileReader(userFile)); //
                User user = new User();                         //tworzę instancję obiektu user
                user.setName(reader.readLine());                //wczytanie z pliku imienia
                user.setSurname(reader.readLine());             //wczytanie z pliku nazwiska
                user.setAge(new Integer(reader.readLine()));    //wczytanie z pliku wieku ze zmianą typu ze stringa na int
                if (reader.readLine().equals(Sex.FEMALE.toString())) { //sprawdzamy co jest w linii z płcią
                    user.setSex(Sex.FEMALE);               //jezeli to female taką wartość ustawiawmy
                } else {
                    user.setSex(Sex.MALE);                 //i wiadomo co w innym przypadku
                }
                userCollection.add(user);                       //takiego użytkownika dodajemy do kolekcja tak by można było go używać w programie
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static void showUsers() {
        System.out.println("wyświetlanie użytkowników");
        if (!userCollection.isEmpty()) {    //sprawdzamy czy kolekcja użytkowników nie jest pusta
            int index = 0;                  //alokujemy index
            //formatowany ciąg znaków pozwala na zarezerwowanie odpowiedniej ilości znaków w linii
            System.out.printf("%-3s %-10s %-10s %-10s\n", "LP.", "IMIĘ", "NAZWISKO", "WIEK");
            //iterujemy prze kolekcję uzytkowników  wypisujący ich index, imie, nazwisko i wiek
            for (User user : userCollection) {
                System.out.printf("%-3s %-10s %-10s %-10s\n", index++, user.getName(), user.getSurname(), user.getAge());
            }
        } else { //jeżeli kolekcja żytkowników jest pusta, to wyświetl komunikat, że lista jest pusta.
            System.out.println("Lista użytkowników jest pusta");
        }
    }

    private static void addUsers(Scanner scanner) {
        User user = new User();                         //zarezerwuj pamięć na obiekt user
        System.out.println("Dodawanie użytkownika");    //Wyświetl na konsoli "dodawnie użytkownika"
        System.out.println("Podaj imię:");              //Wyświetl na konsoli "Podaj imie"
        user.setName(scanner.nextLine());               //Czekaj aż zostanie podany tekst z imieniem
        System.out.println("Podaj nazwisko:");          //Wyświetl na konsoli "Podaj nazwisko"
        user.setSurname(scanner.nextLine());            //Czekaj aż zostanie podany tekst z nazwiskiem
        System.out.println("Podaj wiek");               //Wyświtl na konsoli "podaj wiek"
        user.setAge(new Integer(scanner.nextLine()));   //czekaj aż podany zostanie wiek w postaci liczby
        System.out.println("Sprawdzam płeć..");         //wyświetl na konsoli "sprawdzam płec"
        ifMaleOrFemale(user);                           //wywołuję funkcję która sprawdza płeć i ustawia ją w obiekcie user
        if (user.getSex().equals(Sex.FEMALE)) {    //sprawdzam czy ustawiona płeć to kobieta
            System.out.print("kobieta");                //wyświetla na konsoli "kobieta"
        } else {                                        //jeżeli nie to:
            System.out.print("mężczyzna");               //wyświetl "mężczyzna"
        }
        userCollection.add(user);                       //Dodaj uzupełniony obiekt do kolekcji
        System.out.println();
        System.out.println("użytkownik dodany!");       //wyświetl "użytkownik dodany"
        saveUserToFile(user);
    }

    private static void saveUserToFile(User user) {
        try {//spodziewamy się, że może pójść coś nie tak więc każemy "spróbować" programowi obsłużyć nasz plik
            //zaalokuj pamięć na uchwyt do pliku o nazwie imieNazwisko.usr
            File file = new File(user.getName() + user.getSurname() + ".usr");
            //zaalokuj pamięć na strumień danych do pliku
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
            fileWriter.write(user.getName());                   //wpisz do pliku imie
            fileWriter.newLine();                               //przejdź do nowej linii
            fileWriter.write(user.getSurname());                //wpisz nazwisko do pliku
            fileWriter.newLine();                               //przejdź do nowej linii
            fileWriter.write(Integer.toString(user.getAge()));  //wpisz wiek do pliku
            fileWriter.newLine();                               //przejdź do nowej linii
            fileWriter.write(user.getSex().toString());         //wpisz płeć do pliku
            fileWriter.close();                                 //zamknij strumień
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sprawdza na podstawie imienia, czy podany w parametrze obiekt
     * posiada płeć kobieta czy mężczyzna
     *
     * @param user użytkownik, który ma uzupełnione pole imie.
     */
    private static void ifMaleOrFemale(User user) {
        if (user.getName().endsWith("a")) {         // sprawdzam, czy imię kończy się na "a"
            user.setSex(Sex.FEMALE);           //ustawiam płeć kobieta
        } else {                                    //jeżeli nie to:
            user.setSex(Sex.MALE);             //ustawiam płeć mężczyzna
        }
    }

    /**
     * zapis do pliku
     *
     * @param fileName
     */
    public static void createFile(String fileName) {
        //tworzę uchwyt do pliku
        File file = new File(fileName);
        try { //spodziewam się, że może pójść coś nie tak i zostanie rzucony wyjątek w razie niepowodzenia
            if (!file.exists()) { // sprawdzam, czy plik już istnieje
                file.createNewFile(); // jeżeli nie, to tworzę nowy
            }

            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
            fileWriter.write("pierwsza linia"); //zaczynając pisaćd do pliku piszemy w pierwszej możliwej linii
            fileWriter.newLine(); // przechodzimy do nowej linii
            fileWriter.write("druga linia"); //wpisujemy drugą linię

        } catch (IOException e) {
            e.printStackTrace();//wyświetl stacktrace jeżeli został rzucony jakiś wyjątek
        }
    }
}
