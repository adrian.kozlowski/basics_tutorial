package arrays;

import java.util.Scanner;

public class Table1 {
    public static void main(String[] args) {
        Scanner numbers = new Scanner(System.in);//Tworzymy skaner i mówimy mu skąd ma pobierać dane: klawiatura (system.in)
        int x;
        System.out.print("Podaj dodatnią liczbę całkowitą: ");
        x = numbers.nextInt(); //odczytanie liczby do entera
        if (x > 0) {
            for (int i = 0; i <= x; i++) {
                System.out.println(i);
            }
        } else {
            while (x < 0) {
                System.out.println("Podano nieprawidłową liczbę");
                System.out.print("Podaj dodatnią liczbę całkowitą: ");
                x = numbers.nextInt();
            }
            for (int i = 0; i <= x; i++) {
                System.out.println(i);
            }
        }


    }
}