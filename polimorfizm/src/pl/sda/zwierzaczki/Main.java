package pl.sda.zwierzaczki;

public class Main {
    public static void main(String[] args) {

        Animal cat = new Cat();

        Barkable dog = new Dog();
        listenToBarkables(dog);
    }

    public static void listenToBarkables(Barkable... animals) {
        for (Barkable animal : animals) {
            animal.bark();
        }
    }

    /**
     * metoda odsłuchująca zwierzęta
     *
     * @param animals varargs (dowolna ilość parametrów załadowana do tablicy)
     */
    public static void listenToAnimals(Animal... animals) {
        for (Animal animal : animals) {
            animal.go();
        }
    }
}
