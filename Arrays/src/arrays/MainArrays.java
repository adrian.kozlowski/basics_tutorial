package arrays;

public class MainArrays {


    public static void main(String[] args) {
        //alokujemy tablicę 3-elementową ze slowami Ala ma kota
        String[] arrayOfStrings = new String[]{"Ala", "ma", "kota"};
        //alokujemy NOWĄ!! tablicę z 4 miejscami (będzie wypełniona null-ami)
        String[] czarnyKot = new String[4];
        //przechodzimy (iterujemy) poprzez tablicę arrayOfStrings
        for (int i = 0; i < arrayOfStrings.length; i++) {
            //sprawdzam czy i osiągnęło właściwą wartość czyli 2
            if (i == 2) {
                //jeżli i == 2 to do nowej tablicy gdzie index = 2 wpisuję czarnego
                czarnyKot[i] = "czarnego";
                //do indeksu 2 + 1 wpisuję wartość, któa jest w arrayOfStrings[2]
                czarnyKot[i + 1] = arrayOfStrings[i];
                //przerywam pętlę
                break;
            }
            //normalne zachowanie: przekopiowanie wartości dla każdego innego przypadku
            czarnyKot[i] = arrayOfStrings[i];
        }
        String polecenie =
                "Napisz metodę która sprawdza czy elementy są krótsze od 4 " +
                        "i usuwa je z tablicy a następnie zwraca tablicę z usuniętymi słowami";
        String[] split = polecenie.split(" ");

        czytajTablice(split);
        String[] strings = deleteShortAndRet(split);
        czytajTablice(strings);
    }

    public static String[] deleteShortAndRet(String... args) {
        //sprawdzam jaką długość ma tablica, która jest parametrem
        int length = args.length;
        //inicjuję indeks nowej tablicy
        int newIndex = 0;
        //tworzę nową tablicę o długości length, która zostanie zwrócona
        String[] strings = new String[length];
        //iteruję poprzez tablicę z parametru
        for (int i = 0; i < args.length; i++) {
            //jeżeli napotkam na słowo, które jest krótsze od 4
            if (args[i].length() < 4) {
                //wpisz do nowej tablicy i inkrementuj nowy indeks
                strings[newIndex++] = args[i];
                //inkrementacja (zwiększanie o 1)
            }

        }
        //usunięcie pustych elementów
        String[] retArray = new String[newIndex];
        for (int i = 0; i < newIndex; i++) {
            retArray[i] = strings[i];
        }
        return retArray;
    }

    public static void deleteShort(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() < 4) {
                array[i] = null;
            }
        }
    }

    /**
     * reads array
     *
     * @param arrayToRead
     */
    public static void czytajTablice(String... arrayToRead) {
        for (String s : arrayToRead) {
            System.out.print(s);
            System.out.print(",");
        }
        System.out.println();
    }


}
