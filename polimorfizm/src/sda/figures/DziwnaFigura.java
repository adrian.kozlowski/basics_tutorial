package sda.figures;

/**
 * Created by Adrian on 09.01.2017.
 */
public class DziwnaFigura extends Figura {

    public double obliczObjetosc(double h, double w, double d) throws Exception {
        if (d > 0) {

            return d * h * w;
        }
        throw new Exception("mniejsze od zera");
    }

    public double obliczObwod(double h, double w) {
        try { // spróbuj
            if (h < 0) { //sprawdzić czy h < 0
                throw new Exception(); // jeżeli tak to rzuć wyjątek
            }
            return 2 * h + 2 * w;
        } catch (Exception e) { //jeżeli wyjątek jest złapany
            System.out.println("wyjątek obsłużony");
            return -1;
        } finally {
            System.out.println("finally");
        }
    }

}
