package pl.sda.animals;

/**
 * Created by Adrian on 09.01.2017.
 */
public class Zyrafa extends Zwierze implements Szczekajace, Miauczace {
    @Override
    public void mialcz() {
        System.out.println("miau");
    }

    @Override
    public void szczekaj() {
        System.out.println("szczek");
    }

    @Override
    public void oddychaj() {
        System.out.println("oddycham jak żyrafa");
    }
}
