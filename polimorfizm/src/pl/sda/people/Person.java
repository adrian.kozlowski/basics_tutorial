package pl.sda.people;

/**
 * Created by Adrian on 17.12.2016.
 */
public class Person extends Human {

    public String name = "inicjalizuję w miejscu definicji";
    public int gender;

    public Person(){
        System.out.println("konstruktor");
    }

    @Override
    public void calculateSalary() {
        System.out.println("wypłata obliczona w klasie Person");
        super.calculateSalary();
    }
}
