package pl.sda.cars;

public class Car {
    private Boolean distance = Boolean.TRUE;

    public Car() {
        System.out.println("konstruktor car");
    }

    public Car(String name) {
        System.out.println("Konstruktor car z parametrem " + name);
    }

    /**
     * javadoc opisuje co się dzieje wewnątrz metody
     */
    public void go() {
        System.out.println("samochód jedzie");
    }

    /**
     * Zwróć szczególną uwagę na tę metodę. Zauważ, że słowo break jest kluczowe i żadna metoda
     * nie może się tak nazywać.
     */
    public void breakCar() {
        System.out.println("samochó hamuje");
    }

    public boolean getDistance() {
        return distance;
    }
}
