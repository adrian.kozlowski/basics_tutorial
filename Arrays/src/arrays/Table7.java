package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class Table7 {

    public static void main(String[] args) {
        Table7 go = new Table7();
        go.checkIfPalindrome();
    }

    public void checkIfPalindrome() {
        Scanner palindrome = new Scanner(System.in);
        System.out.print("Podaj slowo: ");
        String slowo = palindrome.nextLine();
        char[] slow2 = slowo.toCharArray();
        char[] slow3 = new char[slow2.length];
        for (int i = 0; i < slow2.length; i++) {
            slow3[i] = slow2[slow2.length - 1 - i];
        }
        if (Arrays.equals(slow2, slow3)) {
            System.out.println("Wpisane słowo: \"" + slowo + "\" jest palindromem");
        } else {
            System.out.println("Wpisane słowo: \"" + slowo + "\" nie jest palindromem");
        }


    }
}