package pl.sda.zwierzaczki;

public class Dog extends Animal implements Barkable {

    public void go() {
        System.out.println("ide na 4 lapkach");
    }

    public void bark() {
        System.out.println("SZCZEK!");
    }
}
