package pl.sda.cars;

public class Sedan extends Car {
    private String name = "domyslna nazwa";

    public Sedan() {
        System.out.println("Konstruktor ze sedana");
    }

    public Sedan(String name) {
        super(name);
        this.name = name;
        System.out.println("Konstruktor z parametrem " + name);
    }

    public void turnOnLights() {
        System.out.println("światła włączone");
    }

    @Override
    public void go() {
        System.out.println("jadę po " + name + "owemu");
    }

    public void go(Integer velocity) {
        System.out.println("teraz jadę z predkością " + velocity + "km/h");
    }
}
