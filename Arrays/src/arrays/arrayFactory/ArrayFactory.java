package arrays.arrayFactory;

public class ArrayFactory {

    private int x;

    public ArrayFactory(int x) {
        this.x = x;
    }

    public static void main(String[] args) {
        ArrayFactory af = new ArrayFactory(7);
        int[] tab1 = af.oneDimension();
        int[][] tab2 = af.twoDimensions();
        System.out.println(tab1);
        System.out.println(tab2);
    }

    public int[] oneDimension() {
        return new int[this.x];
    }

    public int[][] twoDimensions() {
        return new int[this.x][this.x];
    }
}

