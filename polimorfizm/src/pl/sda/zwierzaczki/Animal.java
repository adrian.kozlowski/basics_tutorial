package pl.sda.zwierzaczki;

public abstract class Animal {
    protected int high;

    public int getHigh() {
        return high;
    }

    public Animal setHigh(int high) {
        this.high = high;
        return this;
    }

    public abstract void go();
}
