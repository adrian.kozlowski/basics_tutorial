package sda.dsa;

import sda.Human;

public class OtherClass {

    public void showNumber(int number) {
        number = 10;
        System.out.println(number);
    }

    public void showNumber(float number) {
        number = 11;
        System.out.println(number);
    }

    public void greetHuman(Human person) {

        person.setName("Nie pamiętam imienia");
        System.out.println(person.getName());
    }
}
