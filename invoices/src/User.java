/**
 * Created by Adrian on 10.12.2016.
 */
public class User {


    private String name;
    private String surname;
    private int age;
    private Sex sex; // to pole może przyjąć tylko wartość FEMALE albo MALE - NIE inną.

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return getName();
    }
}
