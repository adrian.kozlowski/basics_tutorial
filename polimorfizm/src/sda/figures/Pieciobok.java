package sda.figures;

/**
 * Created by Adrian on 09.01.2017.
 */
public class Pieciobok extends Figura {
    private float bokC;
    private float bokD;
    private float bokE;

    public Pieciobok(float bokA, float bokB, float bokC, float bokD, float bokE) {
        this.bokA = bokA;
        this.bokB = bokB;
        this.bokC = bokC;
        this.bokD = bokD;
        this.bokE = bokE;
    }

    @Override
    public float pole() throws MniejszaOdZeraException {
        if(bokA > 0){
            return bokA * bokB * bokC * bokD * bokE;
        } else {
            throw  new MniejszaOdZeraException();
        }
    }
}
