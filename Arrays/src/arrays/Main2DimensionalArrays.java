package arrays;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Adrian on 11.01.2017.
 */
public class Main2DimensionalArrays {

    public static void main(String... args) {
        int[][] int2dArray = new int[3][3];

        for (int[] ints : int2dArray) {
            for (int anInt : ints) {
                System.out.print(anInt);
            }
            System.out.println();
        }
        int[][] arrayOfInts = new int[10][10];
        fillArray(arrayOfInts);

        int[][] jakas2WymiarowaTablica = changeValue(arrayOfInts);
        System.out.println(Arrays.deepToString(jakas2WymiarowaTablica));

    }

    public static void fillArray(int[][] args) {
        Random random = new Random();
        for (int i = 0; i < args.length; i++) {
            for (int x = 0; x < args[i].length; x++) {
                args[i][x] = random.nextInt(100);
            }
        }

    }

    public static int[][] changeValue(int[][] args) {
        for (int i = 0; i < args.length; i++) {
            for (int x = 0; x < args[i].length; x++) {
                if (args[i][x] > 50) {
                    args[i][x] = -100;
                }
            }
        }
        return args;
    }
    //000
    //000
    //000
}
