package arrays;

import java.util.Scanner;

public class Table6 {
    public static void main(String[] args) {
        Table6 go = new Table6();
        go.decToBinary();
    }

    public void decToBinary() {
        Scanner binar = new Scanner(System.in);
        System.out.print("podaj liczbe: ");
        int binary = binar.nextInt();
        String resul = "";
        while (binary > 0) {
            Integer c = binary % 2;
            resul = c + resul;
            binary = binary / 2;
        }
        System.out.println(resul);
    }
}