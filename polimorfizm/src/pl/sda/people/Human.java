package pl.sda.people;

/**
 * Created by Adrian on 20.12.2016.
 */
public class Human extends God {

    private int gender;

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    @Override
    public void calculateSalary(){
        super.calculateSalary();
        System.out.println("wypłata obliczona w klasie Human");
    }

}
