package pl.sda.animals;

/**
 * Created by Adrian on 09.01.2017.
 */
public class Kot extends Zwierze implements Miauczace {
    public String glosnoscMruczenia;

    @Override
    public void mialcz() {
        System.out.println("miau");
    }

    @Override
    public void oddychaj() {
        System.out.println("oddycham jak kot");
    }
}
