package sda.figures;

/**
 * Created by Adrian on 09.01.2017.
 */
public class Prostokat extends Figura {

    public Prostokat(float bokA, float bokB) {
        this.bokA = bokA;
        this.bokB = bokB;
    }

    /**
     * Liczy pole kwadratu, sprawdza czy któraś z  długości nie jest krótsza od 0
     *
     * @return
     * @throws Exception kiedy conajmniej jeden z boków jest mniejszy od zera
     */
    @Override
    public float pole() throws MniejszaOdZeraException, RownaZeroException {
        //Sprawzenie wartośći w polach klasy
        if (this.bokA > 0 && this.bokB > 0) {
            return this.bokA * this.bokB; //obliczenia
        } else if (this.bokB == 0 || this.bokA == 0) {
            throw new RownaZeroException();
        } else {
            throw new MniejszaOdZeraException(); // wyrzucenie wyjątku
        }
    }
}
